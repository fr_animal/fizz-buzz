const { join, last } = require('ramda')
const { fizzbuzz } = require('./fizzbuzz.js')
const upto = +last(process.argv)

if (isNaN(upto)) {
  console.error('Invalid input')
} else {
  console.log(join(', ', fizzbuzz(upto)))
}
