const { expect } = require('chai')
const { fizzbuzz } = require('./FizzBuzz')

describe('FizzBuzz', () => {
  it('Should swap out numbers divisible by 3 for "Fizz"', () => {
    expect(fizzbuzz(3)).to.deep.equal([1, 2, 'Fizz'])
  })

  it('Should swap out numbers divisible by 5 for "Buzz"', () => {
    expect(fizzbuzz(5)).to.deep.equal([1, 2, 'Fizz', 4, 'Buzz'])
  })

  it('Should swap out numbers divisible by 3 and 5 for "Fizz Buzz"', () => {
    expect(fizzbuzz(15)).to.deep.equal([1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'Fizz Buzz'])
  })

  it('Should work for bigger numbers', () => {
    expect(fizzbuzz(21)).to.deep.equal(
      [1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'Fizz Buzz', 16, 17, 'Fizz', 19, 'Buzz', 'Fizz']
    )
  })
})
