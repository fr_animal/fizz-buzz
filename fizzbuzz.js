const { map, range } = require('ramda')

const isFizz = n => !(n % 3)
const isBuzz = n => !(n % 5)

const fizzBuzzNumber = number => {
  if (isFizz(number) && isBuzz(number)) return 'Fizz Buzz'
  if (isFizz(number)) return 'Fizz'
  if (isBuzz(number)) return 'Buzz'
  return number
}

const fizzbuzz = upto => map(fizzBuzzNumber, range(1, upto + 1))

module.exports = {
  fizzbuzz
}
